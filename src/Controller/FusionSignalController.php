<?php

namespace Drupal\search_api_fusion\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\search_api\ServerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Click signal fusion controller.
 */
class FusionSignalController extends ControllerBase {

  /**
   * Click signal request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $request;

  /**
   * Constructs FusionSignalController object.
   */
  public function __construct(Request $request) {
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): FusionSignalController {
    return new self(
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Send click signal to Fusion.
   *
   * @param \Drupal\search_api\ServerInterface $search_api_server
   *   Search API server object.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   No content response object.
   *
   * @throws \Drupal\search_api\SearchApiException
   */
  public function send(ServerInterface $search_api_server) {
    /** @var \Drupal\search_api_solr\Plugin\search_api\backend\SearchApiSolrBackend $backend */
    $backend = $search_api_server->getBackend();
    /** @var \Drupal\search_api_fusion\Plugin\SolrConnector\FusionConnector $connector */
    $connector = $backend->getSolrConnector();

    // Get query string and filter it in case users send unauthorized
    // query parameters.
    // We could also deny the request in case mandatory parameters are missing
    // (e.g. doc_id) although it is not clear from the documentation which
    // fields are mandatory.
    // @see https://doc.lucidworks.com/fusion-ai/4.2/466/signals-types-and-structures#click
    // We could also validate that the request came from Drupal by passing a
    // CSRF token but this currently fails for anonymous users.
    // @see https://www.drupal.org/docs/8/api/routing-system/access-checking-on-routes/csrf-access-checking#s-anonymoususers
    $params = $this->request->query->all();
    $params['app_id'] = $search_api_server->getOriginalId();
    $authorized_params = [
      'fusion_query_id' => 1,
      'session' => 1,
      'query' => 1,
      'ctype' => 1,
      'ip_address' => 1,
      'doc_id' => 1,
      'doc_ids_s' => 1,
      'url' => 1,
      'app_id' => 1,
      'res_pos' => 1,
      'filter' => 1,
      'filter_field' => 1,
    ];
    $params = array_intersect_key($params, $authorized_params);

    // Send request to Fusion.
    $connector->sendSignal('click', $params);

    $headers = [
      'Cache-Control' => 'no-cache, no-store, must-revalidate',
      'Pragma' => 'no-cache',
      'Expires' => '0',
    ];
    return new Response('', Response::HTTP_NO_CONTENT, $headers);
  }

}
