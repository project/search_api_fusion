<?php

namespace Drupal\search_api_fusion\Plugin\SolrConnector;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api\Query\ConditionInterface;
use Drupal\search_api\Query\QueryInterface as SearchApiQueryInterface;
use Drupal\search_api\SearchApiException;
use Drupal\search_api_solr\Plugin\SolrConnector\BasicAuthSolrCloudConnector;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Solarium\Core\Client\Adapter\Http;
use Solarium\Core\Client\Endpoint;
use Solarium\Core\Query\QueryInterface;
use Solarium\QueryType\Select\Query\Query;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Fusion connector.
 *
 * @SolrConnector(
 *   id = "fusion",
 *   label = @Translation("Fusion"),
 *   description = @Translation("A connector for Lucidworks Fusion.")
 * )
 */
class FusionConnector extends BasicAuthSolrCloudConnector {

  /**
   * Http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected Client $httpClient;

  /**
   * Current active request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $currentActiveRequest;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    $plugin = parent::create($container, $configuration, $plugin_id,
      $plugin_definition);
    $plugin->setLogger($container->get('logger.channel.search_api_fusion'));

    // Set http client used to send signals to fusion.
    $plugin->setHttpClient($container->get('http_client'));
    $plugin->setCurrentActiveRequest($container->get('request_stack')
      ->getCurrentRequest());

    return $plugin;
  }

  /**
   * Set Http client.
   *
   * @param \GuzzleHttp\Client $httpClient
   *   Http client.
   */
  public function setHttpClient(Client $httpClient): void {
    $this->httpClient = $httpClient;
  }

  /**
   * Set Current active request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Current active request.
   */
  public function setCurrentActiveRequest(Request $request) : void {
    $this->currentActiveRequest = $request;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'port' => '8764',
      'fusion_qprofile_search' => '',
      'fusion_click_signals' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state,
  ): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Rename titles and descriptions to reference Fusion instead of Solr.
    $form['host']['#title'] = $this->t('Fusion host');
    $form['host']['#description'] = $this->t('The host name or IP of your Fusion server, e.g. localhost or www.example.com.');

    $form['port']['#title'] = $this->t('Fusion port');
    $form['port']['#description'] = $this->t('The standard Fusion port is 8764.');

    $form['core']['#title'] = $this->t('Fusion app');
    $form['core']['#description'] = $this->t('The name of the Fusion app.');

    // Set default context to /api/solr (Fusion's Solr API).
    $form['context'] = [
      '#type' => 'value',
      '#value' => '/api/solr',
    ];

    // Extra fields specific to Fusion.
    $form['fusion_qprofile_search'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Fusion search query profile'),
      '#description' => $this->t('The Fusion query profile to use when searching.'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['fusion_qprofile_search'] ?? '',
    ];

    $form['fusion_click_signals'] = [
      '#type' => 'checkbox',
      '#title' => 'Send click signals to Fusion',
      '#description' => $this->t('<a href="@url_click">Click signals</a> are sent after users click on a search result.',
        [
          '@url_click' => 'https://doc.lucidworks.com/fusion-ai/4.2/466/signals-types-and-structures#click',
        ]),
      '#default_value' => $this->configuration['fusion_click_signals'] ?? FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewSettings(): array {
    $info = parent::viewSettings();
    $info[] = [
      'label' => $this->t('Send click signals to Fusion'),
      'info' => ($this->configuration['fusion_click_signals'] ? $this->t("Enabled") : $this->t("Disabled")),
    ];

    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public function pingServer() {
    // SolrConnectorPluginBase pings Fusion on admin/info/system which may not
    // exist. Ping Fusion on admin/ping instead.
    return $this->pingCore();
  }

  /**
   * {@inheritdoc}
   */
  public function reloadCore(): bool {
    // Not applicable to Fusion, just return success.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   *
   * Send autocomplete requests to the Query profile:
   *   /api/apps/<app>/query/<autocomplete-query-profile>
   *
   * Instead of the default:
   *   /api/solr/<app>/select
   */
  public function search(Query $query, ?Endpoint $endpoint = NULL) {
    return $this->queryProfile($query,
      $this->configuration['fusion_qprofile_search']);
  }

  /**
   * Send request to a Fusion query profile.
   *
   * @param \Solarium\Core\Query\QueryInterface $query
   *   Query to send to the the query profile.
   * @param string $query_profile
   *   ID of Fusion query profile.
   *
   * @return \Solarium\Core\Client\Response
   *   The Solarium response object.
   *
   * @throws \Drupal\search_api_solr\SearchApiSolrException
   *
   * @see https://doc.lucidworks.com/fusion/5.5/361/query-api
   */
  public function queryProfile(QueryInterface $query, $query_profile) {
    $this->connect();

    if ($this->configuration['fusion_click_signals']) {
      // Change adapter to Http, so we can get 'x-fusion-query-id' header in
      // the query response. The default Curl adapter does not return headers.
      $adapter = new Http();
      $this->solr->setAdapter($adapter);
    }

    // Set context to /api/apps (instead of the default /api/solr which is set
    // in the admin form).
    $apps_endpoint = $this->createEndpoint('search_api_fusion_apps',
      ['context' => 'api/apps']);

    // Set handler to /query/<query-profile>.
    $query->setHandler('query/' . $query_profile);

    // Send request and return response.
    $request = $this->solr->createRequest($query);
    return $this->executeRequest($request, $apps_endpoint);
  }

  /**
   * Send a signal to Fusion.
   *
   * @param string $type
   *   Type of signal.
   * @param array $params
   *   Signal parameters.
   * @param int $timeout
   *   Timeout when sending request to Fusion.
   *   We do not want signal requests to slow down too much the user
   *   experience, in case Fusion happens to be slow. As a result we set a low
   *   timeout (5 seconds instead of 30 seconds by default).
   *
   * @throws \Drupal\search_api\SearchApiException
   *
   * @see https://doc.lucidworks.com/fusion-ai/4.2/466/signals-types-and-structures
   */
  public function sendSignal(string $type, array $params, int $timeout = 5): void {

    // Route signal requests to the Signals API (api/signals/<app>).
    $apps_endpoint = $this->createEndpoint('search_api_fusion_signals',
      ['path' => '/api/signals']);
    $data = [
      [
        'timestamp' => intval(microtime(TRUE) * 1000),
        'type' => $type,
        'params' => $params + [
          'ip_address' => $this->currentActiveRequest->getClientIp(),
          'referrer' => $this->currentActiveRequest->server->get('REFERER'),
        ],
      ],
    ];
    $options = [
      'method' => 'POST',
      'auth' => array_values($apps_endpoint->getAuthentication()),
      'timeout' => $timeout,
      'json' => $data,
    ];

    // Send signal and return response.
    $url = $apps_endpoint->getServerUri() . $apps_endpoint->getCore();
    try {
      /** @var \GuzzleHttp\Psr7\Response $response */
      $response = $this->httpClient->request('POST', $url, $options);

      $code = $response->getStatusCode();
      if ($code != 204) {
        $this->getLogger()
          ->error('Unable to send request signal to Fusion: received status code @code, expected 204.',
            ['@code' => $code]);
        throw new SearchApiException("Signal sent to $url returned HTTP code $code. Data posted: " . json_encode($data));

      }
    }
    catch (GuzzleException $e) {
      $this->getLogger()
        ->error('Unable to send request signal to Fusion: Exception: @message.',
          ['@message' => $e->getMessage()]);
    }
  }

  /**
   * Generate signal parameters.
   *
   * Parameters "filter" and "filter_field" based on which facets are active.
   *
   * @param \Drupal\search_api\Query\QueryInterface $query
   *   Search query.
   *
   * @return array
   *   Array with two keys: "filter" and "filter_field" with values expected by
   *   Fusion. Empty array if no facet is active.
   *
   * @see https://doc.lucidworks.com/fusion-ai/4.2/466/signals-types-and-structures
   */
  public function getSignalParamsFilter(
    SearchApiQueryInterface $query,
  ): array {
    $params = [];
    $filters = $query->getConditionGroup()->getConditions();
    foreach ($filters as $filter) {
      foreach ($filter->getConditions() as $condition) {
        if (!$condition instanceof ConditionInterface) {
          // Fusion's signals API has no documentation about how to deal with
          // complex filters (e.g. [X AND Y] OR Z) so we just ignore those.
          continue;
        }

        $field = $condition->getField();
        $value = $condition->getValue();
        $operator = $condition->getOperator();
        if ($operator != '=') {
          $facet_options = $query->getOption('search_api_facets');
          // Support facets for missing values.
          // Operator for missing values is "<>".
          // For missing values the query consists of multiple conditions,
          // which exclude all other facet values.
          // @see \Drupal\facets\Plugin\facets\query_type\SearchApiString::execute() which creates the conditions.
          // Those generate an fq with multiple conditions and a minus sign
          // e.g. fq= {!tag=facet:facet_field_ss}((*:* -facet_field_ss:"val1") (*:* -facet_field_ss:"val2")).
          // @see \Drupal\search_api_solr\Plugin\search_api\backend\SearchApiSolrBackend::createFilterQueries()
          // @see \Drupal\search_api_solr\Plugin\search_api\backend\SearchApiSolrBackend::createFilterQuery()
          if (!empty($facet_options[$field]['missing'])) {
            if (!empty($params['filter_field']) && in_array($field,
                $params['filter_field'])) {
              // Missing value filter was already added.
              continue;
            }
            $params['filter_field'][] = $field;
            // Fusion's signals API does not support filters made of multiple
            // conditions and requires those to be formatted like
            // this: myfield/myvalue.
            // @see https://doc.lucidworks.com/fusion-ai/4.2/466/signals-types-and-structures#request.
            // As a workaround we use the bang value (!) which is used by Drupal
            // and seems to work with Fusion.
            // @see SearchApiSolrBackend::extractFacets()
            // Response events do show up with the two conditions, albeit with a
            // missing parenthesis e.g. (*:* AND -myfield:[* TO *]
            $params['filter'][] = "$field/!";
          }
          else {
            // Fusion's signals API has no documentation about how to deal with
            // operators other than '=' so we just ignore those.
            continue;
          }

        }
        else {
          // It is not necessary to escape slashes in $value -- Fusion will
          // properly parse the value.
          $params['filter_field'][] = $field;
          $params['filter'][] = $field . '/' . $value;
        }
      }
    }
    return $params;
  }

}
