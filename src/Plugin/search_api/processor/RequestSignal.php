<?php

namespace Drupal\search_api_fusion\Plugin\search_api\processor;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Controller\TitleResolver;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\search_api\LoggerTrait;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Query\ResultSetInterface;
use Drupal\search_api\SearchApiException;
use Drupal\search_api_solr\SearchApiSolrException;
use Drupal\search_api_solr\Utility\Utility;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Processor to send a request signal to Fusion's /signal endpoint.
 *
 * @SearchApiProcessor(
 *   id = "fusion_request_signal",
 *   label = @Translation("Send request signal to Fusion"),
 *   description = @Translation("Sends a <a href=':url'>request signal</a> to Fusion after search results are returned.",
 *   arguments = {":url" = "https://doc.lucidworks.com/fusion-ai/4.2/466/signals-types-and-structures#request"}),
 *   stages = {
 *     "postprocess_query" = 0,
 *   }
 * )
 */
class RequestSignal extends ProcessorPluginBase {

  use LoggerTrait;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected AccountProxy $currentUser;

  /**
   * Current active request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $currentActiveRequest;

  /**
   * Route match used to get page title.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * Title resolver used to get page title.
   *
   * @var \Drupal\Core\Controller\TitleResolver
   */
  protected TitleResolver $titleResolver;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    /** @var static $processor */
    $processor = parent::create($container, $configuration, $plugin_id,
      $plugin_definition);

    $processor->setLogger($container->get('logger.channel.search_api_fusion'));
    $processor->setCurrentUser($container->get('current_user'));
    $processor->setCurrentActiveRequest($container->get('request_stack')
      ->getCurrentRequest());
    $processor->setRouteMatch($container->get('current_route_match'));
    $processor->setTitleResolver($container->get('title_resolver'));

    return $processor;
  }

  /**
   * {@inheritdoc}
   */
  public function postprocessSearchResults(ResultSetInterface $results) : void {

    if (!$this->currentUser->hasPermission('send signals to any fusion server')) {
      return;
    }

    /** @var \Drupal\search_api\Query\Query $query */
    $query = $results->getQuery();
    try {
      /** @var \Drupal\search_api\Entity\Server $server */
      $server = $query->getIndex()->getServerInstance();
      /** @var \Drupal\search_api_solr\Plugin\search_api\backend\SearchApiSolrBackend $backend */
      $backend = $server->getBackend();
    }
    catch (SearchApiException $e) {
      $this->getLogger()
        ->error('Failed to retrieve Fusion solr connector config when attempting to send a request signal: Exception: @message.',
          ['@message' => $e->getMessage()]);
      return;
    }

    $keys = $query->getOriginalKeys();
    if (is_array($keys)) {
      try {
        $keys = Utility::flattenKeys($keys);
      }
      catch (SearchApiSolrException $e) {
        $this->getLogger()
          ->error('Failed to flatten search keys into a single search string when attempting to send a request signal: Exception: @message.',
            ['@message' => $e->getMessage()]);
        return;
      }
    }

    try {
      /** @var \Drupal\search_api_fusion\Plugin\SolrConnector\FusionConnector $connector */
      $connector = $backend->getSolrConnector();
    }
    catch (PluginException | SearchApiException $e) {
      $this->getLogger()
        ->error('Failed to load Fusion connector when attempting to send a request signal: Exception: @message.',
          ['@message' => $e->getMessage()]);
      return;
    }

    $params = $connector->getSignalParamsFilter($query) + [
      'query' => $keys,
      'page_title' => $this->titleResolver->getTitle($this->currentActiveRequest,
          $this->routeMatch->getRouteObject()),
      'app_id' => $server->getOriginalId(),
    ];

    $connector->sendSignal('request', $params);
  }

  /**
   * Sets the current user.
   *
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   The current user.
   *
   * @return $this
   */
  public function setCurrentUser(AccountProxy $current_user): RequestSignal {
    $this->currentUser = $current_user;
    return $this;
  }

  /**
   * Set Current active request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Current active request.
   */
  public function setCurrentActiveRequest(Request $request) : void {
    $this->currentActiveRequest = $request;
  }

  /**
   * Set route matching result.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   Route match object.
   */
  public function setRouteMatch(RouteMatchInterface $routeMatch): void {
    $this->routeMatch = $routeMatch;
  }

  /**
   * Set title resolver object.
   *
   * @param \Drupal\Core\Controller\TitleResolver $titleResolver
   *   Object which gets the title for a given route.
   */
  public function setTitleResolver(TitleResolver $titleResolver): void {
    $this->titleResolver = $titleResolver;
  }

}
