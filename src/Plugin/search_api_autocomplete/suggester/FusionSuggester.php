<?php

namespace Drupal\search_api_fusion\Plugin\search_api_autocomplete\suggester;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Utility\Error;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\LoggerTrait;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api_autocomplete\SearchInterface;
use Drupal\search_api_autocomplete\Suggester\SuggesterPluginBase;
use Drupal\search_api_autocomplete\Suggestion\SuggestionFactory;
use Drupal\search_api_fusion\Plugin\SolrConnector\FusionConnector;
use Drupal\search_api_solr\SolrBackendInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Retrieve suggestions from a Fusion query profile.
 *
 * @SearchApiAutocompleteSuggester(
 *   id = "fusion",
 *   label = @Translation("Fusion query profile"),
 *   description = @Translation("Retrieve suggestions from a Fusion query profile."),
 * )
 */
class FusionSuggester extends SuggesterPluginBase implements PluginFormInterface {

  use LoggerTrait;
  use PluginFormTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $plugin */
    $plugin = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $plugin->setLogger($container->get('logger.channel.search_api_fusion'));
    return $plugin;
  }

  /**
   * Retrieves the logger.
   *
   * @return \Psr\Log\LoggerInterface
   *   The logger.
   */
  public function getLogger() {
    return $this->logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function supportsSearch(SearchInterface $search) {
    return (bool) static::getFusionConnector($search->getIndex());
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'fusion_qprofile_autocomplete' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['fusion_qprofile_autocomplete'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Fusion autocomplete query profile'),
      '#description' => $this->t('The Fusion query profile to use when generating autocomplete suggestions.'),
      '#required' => TRUE,
      '#default_value' => $this->getConfiguration()['fusion_qprofile_autocomplete'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getAutocompleteSuggestions(QueryInterface $query, $incomplete_key, $user_input) {
    // Make sure we have a valid Fusion connector.
    $connector = static::getFusionConnector($query->getIndex());
    if (!$connector) {
      return [];
    }

    // Fetch suggestions from query profile:
    // /api/apps/<app>/query/<autocomplete-query-profile>
    // We do not sanitize $user_input ourselves as Solarium takes care of this.
    $solarium_query = $connector->getAutocompleteQuery();
    $solarium_query->addParam('q', $user_input);
    $solarium_query->addParam('rows', $query->getOption('limit', 10));
    $configuration = $this->getConfiguration();
    $response = $connector->queryProfile($solarium_query, $configuration['fusion_qprofile_autocomplete']);
    if ($response->getStatusCode() != 200) {
      $this->getLogger()->error('Unable to fetch suggestions from query profile: received status code @code, expected 200.', ['@code' => $response->getStatusCode()]);
      return [];
    }
    $results = Json::decode($response->getBody());

    // Build and return list of suggestions.
    $suggestions = [];
    $factory = new SuggestionFactory($user_input);
    foreach ($results['response']['docs'] as $result) {
      $suggestions[] = $factory->createFromSuggestedKeys($result['query']);
    }
    return $suggestions;
  }

  /**
   * Get Fusion connector from index.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   Search index to check for Fusion connector.
   *
   * @return \Drupal\search_api_fusion\Plugin\SolrConnector\FusionConnector|null
   *   Fusion connector, or NULL if this index does not use a Fusion connector.
   *
   * @see Drupal\search_api_solr_autocomplete\Plugin\search_api_autocomplete\suggester\BackendTrait::getBackend()
   */
  public static function getFusionConnector(IndexInterface $index) {
    try {
      if (!$index->hasValidServer()) {
        return NULL;
      }

      $server = $index->getServerInstance();
      $backend = $server->getBackend();
      if (!$backend instanceof SolrBackendInterface) {
        return NULL;
      }
      $connector = $backend->getSolrConnector();
      if (!$connector instanceof FusionConnector) {
        return NULL;
      }

      return $connector;
    }
    catch (\Exception $e) {
      $logger = \Drupal::logger('search_api_fusion');
      Error::logException($logger, $e);
    }
    return NULL;
  }

}
