<?php

namespace Drupal\search_api_fusion\Plugin\views\area;

use Drupal\Core\Link;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Url;
use Drupal\views\Plugin\views\area\AreaPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an area for spellcheck suggestions.
 *
 * @ViewsArea("search_api_fusion_spellcheck")
 *
 * @see Drupal\search_api_spellcheck\Plugin\views\area\DidYouMeanSpellCheck
 */
class FusionSpellcheck extends AreaPluginBase {

  /**
   * The current query parameters.
   *
   * @var array
   */
  protected $currentQuery;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * Constructs a FusionSpellcheck object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   The current path.
   */
  final public function __construct(array $configuration, $plugin_id, $plugin_definition, CurrentPathStack $current_path) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentPath = $current_path;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('path.current'),
    );
  }

  /**
   * Render the area.
   *
   * @param bool $empty
   *   (optional) Indicator if view result is empty or not. Defaults to FALSE.
   *
   * @return array
   *   In any case we need a valid Drupal render array to return.
   */
  public function render($empty = FALSE) {
    /** @var \Drupal\search_api\Plugin\views\query\SearchApiQuery */
    $query = $this->query;
    /** @var \Drupal\search_api\Query\ResultSetInterface */
    $results = $query->getSearchApiResults();

    // Return nothing if Fusion provided no spellcheck suggestion.
    $response = $results->getExtraData('search_api_solr_response');
    if (empty($response['spellcheck']['suggestions'])
      || !is_array($response['spellcheck']['suggestions'])) {
      return [];
    }

    $suggestions = $response['spellcheck']['suggestions'];
    $best_suggestions = [];
    foreach ($suggestions as $delta => &$data) {
      // Ensure the suggestion array is defined. This skips string elements in
      // the array that solely contain a search term.
      if (isset($data['suggestion']) && !empty($data['suggestion'])) {
        // Order Fusion provided spellcheck suggestions by frequency.
        usort($data['suggestion'],
          function ($suggestion1, $suggestion2): int {
            return $suggestion1['freq'] <=> $suggestion2['freq'];
          });
        // The original search term is in the array element previous to the
        // suggestions.
        $search_term = $suggestions[$delta - 1];
        // Extract suggested word with the highest frequency (last array
        // element). Use the original search term as the array key.
        $best_suggestions[$search_term] = $data['suggestion'][array_key_last($data['suggestion'])]['word'];
      }
    }

    if (empty($best_suggestions)) {
      return [];
    }

    $link = $this->getSuggestionLink($best_suggestions);
    if (is_null($link)) {
      return [];
    }
    // Return hyperlink "Did you mean: <best spellcheck suggestion> ?".
    return [
      '#theme' => 'search_api_fusion_spellcheck',
      '#label' => $this->t('Did you mean:'),
      '#link' => $link,
    ];
  }

  /**
   * Gets the suggestion link.
   *
   * @param array $suggestions
   *   Array of suggested spellcheck words keyed by the original search terms.
   *
   * @return \Drupal\Core\Link|null
   *   The suggestion link or NULL if the query is not set or the suggested
   *   text matches the original search terms.
   */
  protected function getSuggestionLink(array $suggestions) {
    if ($this->view->hasUrl()) {
      $path = '/' . $this->view->getPath();
    }
    else {
      $path = $this->currentPath->getPath($this->view->getRequest());
    }

    $currentQuery = $this->getCurrentQuery();
    unset($currentQuery['q']);
    if (!isset($currentQuery['query'])) {
      return NULL;
    }

    // Search and replace the original search terms with the returned suggested
    // words. This way the original search terms without a spelling suggestion
    // are preserved.
    // Use the original search term(s) as a default.
    $suggested_text = $currentQuery['query'];
    foreach ($suggestions as $original_term => $suggested_term) {
      // Replace only full words and not part of a word.
      $suggested_text = preg_replace(
        '/\b' . $original_term . '\b/u',
        $suggested_term,
        $suggested_text
      );
    }

    // Handle the rare possibility of the original search term(s) matching the
    // suggested text.
    if ($suggested_text == $currentQuery['query']) {
      return NULL;
    }

    $url = Url::fromUserInput($path, [
      'query' => [
        'query' => $suggested_text,
      ] + $currentQuery,
    ]);

    return Link::fromTextAndUrl($suggested_text, $url);
  }

  /**
   * Gets the current query parameters.
   *
   * @return array
   *   Key value of parameters.
   */
  public function getCurrentQuery(): array {
    if (NULL === $this->currentQuery) {
      $this->currentQuery = $this->view->getRequest()->query->all();
    }
    return $this->currentQuery;
  }

}
