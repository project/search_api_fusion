<?php

namespace Drupal\search_api_fusion\Plugin\views\area;

use Drupal\views\Plugin\views\area\AreaPluginBase;

/**
 * Provides an area for landing pages.
 *
 * @ViewsArea("search_api_fusion_landing_pages")
 */
class FusionLandingPages extends AreaPluginBase {

  /**
   * Render the area.
   *
   * @param bool $empty
   *   (optional) Indicator if view result is empty or not. Defaults to FALSE.
   *
   * @return array
   *   In any case we need a valid Drupal render array to return.
   */
  public function render($empty = FALSE) {
    /** @var \Drupal\search_api\Plugin\views\query\SearchApiQuery */
    $query = $this->query;
    /** @var \Drupal\search_api\Query\ResultSetInterface */
    $results = $query->getSearchApiResults();

    // Return nothing if Fusion provided no landing page.
    $response = $results->getExtraData('search_api_solr_response');
    if (empty($response['fusion']['landing-pages'])) {
      return [];
    }

    // Extract landing pages.
    $landing_pages = [];
    if (!empty($response['fusion']['landing-pages-extracted'])) {
      $landing_pages = $response['fusion']['landing-pages-extracted'];
    }
    else {
      foreach ($response['fusion']['landing-pages'] as $landing_page) {
        [$url, $title] = explode('$$$', $landing_page);
        $landing_pages[$url] = [
          'title' => $title,
          'url' => $url,
        ];
      }
    }

    // Return render array with landing pages.
    return [
      '#theme' => 'search_api_fusion_landing_pages',
      '#landing_pages' => $landing_pages,
    ];
  }

}
