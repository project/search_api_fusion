<?php

namespace Drupal\search_api_fusion\Plugin\views\field;

use Drupal\search_api\Plugin\views\field\SearchApiStandard;

use Drupal\views\ResultRow;

/**
 * Provides a default handler for fields in Search API Fusion Views.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("search_api")
 */
class SearchApiFusionStandard extends SearchApiStandard {

  /**
   * {@inheritdoc}
   */
  protected function getItemUrl(ResultRow $row, $i) {
    $url = parent::getItemUrl($row, $i);
    // Add ping attribute to Url, which is the click signal url.
    // See SearchApiFusionSubscriber::addClickSignalUrl().
    $ping = $row->_item->getExtraData('ping');
    if ($ping) {
      $attributes = $url->getOption('attributes');
      $attributes['ping'] = $ping->toString();
      $url->setOption('attributes', $attributes);
    }

    return $url;
  }

}
