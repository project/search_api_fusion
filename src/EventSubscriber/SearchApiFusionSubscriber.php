<?php

namespace Drupal\search_api_fusion\EventSubscriber;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Url;
use Drupal\search_api\LoggerTrait;
use Drupal\search_api\SearchApiException;
use Drupal\search_api_solr\Event\PostExtractResultsEvent;
use Drupal\search_api_solr\Event\SearchApiSolrEvents;
use Drupal\search_api_solr\SearchApiSolrException;
use Drupal\search_api_solr\Utility\Utility;
use GuzzleHttp\Utils;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Search api fusion event listener / subscriber.
 */
class SearchApiFusionSubscriber implements EventSubscriberInterface {

  use LoggerTrait;

  /**
   * Constructs a SearchApiFusionSubscriber object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger channel.
   */
  public function __construct(LoggerChannelInterface $logger) {
    $this->setLogger($logger);
  }

  /**
   * Populate results with URL of click signal (if signals are enabled).
   *
   * @param \Drupal\search_api_solr\Event\PostExtractResultsEvent $event
   *   Post extract result event.
   */
  public function addClickSignalUrl(PostExtractResultsEvent $event) : void {

    $search_api_result_set = $event->getSearchApiResultSet();
    $solarium_result = $event->getSolariumResult();
    $query = $event->getSearchApiQuery();

    // Check if this is a fusion connector and if fusion signals are enabled.
    try {
      $config = $query->getIndex()->getServerInstance()->getBackendConfig();
    }
    catch (SearchApiException $e) {
      $this->getLogger()
        ->error('Failed to retrieve Fusion solr connector config: Exception: @message.',
          ['@message' => $e->getMessage()]);
      return;
    }
    if ($config['connector'] != 'fusion') {
      return;
    }
    if (empty($config['connector_config']['fusion_click_signals'])) {
      return;
    }
    // Get original search query.
    $keys = $query->getOriginalKeys();
    if (is_array($keys)) {
      try {
        $keys = Utility::flattenKeys($keys);
      }
      catch (SearchApiSolrException $e) {
        $this->getLogger()
          ->error('Failed to flatten search keys into a single search string: Exception: @message.',
            ['@message' => $e->getMessage()]);
        return;
      }
    }

    // Get Fusion Query ID.
    $headers = Utils::headersFromLines($solarium_result->getResponse()->getHeaders());
    $fusion_query_id = current($headers['x-fusion-query-id']) ?? NULL;

    try {
      // Get signal parameters for filters.
      /** @var \Drupal\search_api_solr\Plugin\search_api\backend\SearchApiSolrBackend $backend */
      $backend = $query->getIndex()
        ->getServerInstance()
        ->getBackend();
      /** @var \Drupal\search_api_fusion\Plugin\SolrConnector\FusionConnector $connector */
      $connector = $backend->getSolrConnector();
      $params_filter = $connector->getSignalParamsFilter($query);

      $extra_data_solr_response = $search_api_result_set->getExtraData('search_api_solr_response');

      // If landing pages present in response.
      if (!empty($extra_data_solr_response['fusion']['landing-pages'])) {
        // Set ping URL for landing pages.
        foreach ($extra_data_solr_response['fusion']['landing-pages'] as $landing_page) {
          [$url, $title] = explode('$$$', $landing_page);
          // Landing pages do not seem to require res_pos to be set.
          $params = $params_filter + [
            'query' => $keys,
            'ctype' => 'landingpage',
            'fusion_query_id' => $fusion_query_id,
            'doc_id' => $url,
            'doc_ids_s' => $url,
            'url' => $url,
          ];
          $extra_data_solr_response['fusion']['landing-pages-extracted'][$url] =
            [
              'title' => $title,
              'url' => $url,
              'ping' => Url::fromRoute("search_api_fusion.signal.click",
                ['search_api_server' => $query->getIndex()->getServerId()],
                ['query' => $params])->toString(),
            ];
        }
        $search_api_result_set->setExtraData('search_api_solr_response',
          $extra_data_solr_response);
      }

      // Set ping URL in each result.
      $start = $extra_data_solr_response['response']['start'] ?? 0;
      $local_pos = 0;
      foreach ($search_api_result_set as $one_result) {
        $local_pos++;
        $search_api_solr_document_fields = $one_result->getExtraData('search_api_solr_document')
          ->getFields();
        $params = $params_filter + [
          'query' => $keys,
          'ctype' => 'result',
          'res_pos' => $start + $local_pos,
          'fusion_query_id' => $fusion_query_id,
          'doc_id' => $search_api_solr_document_fields['id'],
          'doc_ids_s' => $search_api_solr_document_fields['id'],
          'url' => $search_api_solr_document_fields['id'],
        ];

        $one_result->setExtraData('ping',
          Url::fromRoute("search_api_fusion.signal.click",
            ['search_api_server' => $query->getIndex()->getServerId()],
            ['query' => $params]));
        $search_api_result_set->addResultItem($one_result);
      }
    }
    catch (SearchApiException $e) {
      $this->getLogger()
        ->error('Unable to get Fusion click signal parameters: Exception: @message.',
          ['@message' => $e->getMessage()]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[SearchApiSolrEvents::POST_EXTRACT_RESULTS][] = ['addClickSignalUrl'];

    return $events;
  }

}
