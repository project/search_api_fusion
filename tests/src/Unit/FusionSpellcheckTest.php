<?php

namespace Drupal\Tests\search_api_fusion\Unit;

use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\search_api\Plugin\views\query\SearchApiQuery;
use Drupal\search_api\Query\ResultSetInterface;
use Drupal\search_api_fusion\Plugin\views\area\FusionSpellcheck;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;

/**
 * @coversDefaultClass \Drupal\search_api_fusion\Plugin\views\area\FusionSpellcheck
 * @group search_api_fusion
 */
class FusionSpellcheckTest extends UnitTestCase {

  /**
   * Mock of Drupal container object.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $container;

  /**
   * Symfony request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $viewRequestStub;

  /*
   * @todo Mock ParameterBag.
   * @var \Symfony\Component\HttpFoundation\ParameterBag\PHPUnit\Framework\MockObject|MockObject
   */

  /**
   * View mock.
   *
   * @var \Drupal\views\ViewExecutable|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $viewMock;

  /**
   * Mock of current path, used for testing.
   *
   * @var \Drupal\Core\Path\CurrentPathStack|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $path;

  /**
   * FusionSpellcheck instance being tested.
   *
   * @var \Drupal\search_api_fusion\Plugin\views\area\FusionSpellcheck
   */
  protected $fusionSpellcheck;

  /**
   * Mock of result set.
   *
   * @var \Drupal\search_api\Query\ResultSetInterface[\Drupal\search_api\Item\ItemInterface]|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $resultSet;

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    $this->container = $this->getMockBuilder(ContainerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    \Drupal::setContainer($this->container);
    $this->container->method('get')
      ->with('path.validator')
      ->willReturn($this->createStub(PathValidatorInterface::class));

    $this->path = $this->getMockBuilder(CurrentPathStack::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->path->method('getPath')
      ->willReturn('/search');

    $this->fusionSpellcheck = new FusionSpellcheck(['configuration array'], 'plugin_id', [], $this->path);

    $this->viewMock = $this->getMockBuilder('\Drupal\views\ViewExecutable')
      ->disableOriginalConstructor()
      ->getMock();
    $this->fusionSpellcheck->init($this->viewMock, $this->createStub(DisplayPluginBase::class));

    $this->fusionSpellcheck->setStringTranslation($this->createStub(TranslationInterface::class));

    $this->fusionSpellcheck->query = $this->getMockBuilder(SearchApiQuery::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->resultSet = $this->getMockBuilder(ResultSetInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->fusionSpellcheck->query
      ->method('getSearchApiResults')
      ->willReturn($this->resultSet);

    $this->viewRequestStub = $this->getMockBuilder(Request::class)
      ->disableOriginalConstructor()
      ->getMock();

    /** @var \Symfony\Component\HttpFoundation\ParameterBag|\PHPUnit\Framework\MockObject\MockObject $queryMock */
    $queryMock = $this->getMockBuilder(ParameterBag::class)
      ->disableOriginalConstructor()
      ->getMock();
    $this->viewRequestStub->query = $queryMock;
  }

  /**
   * Test render spellcheck returns a suggestion with the highest frequency.
   */
  public function testRenderSpellcheckSuggestion(): void {
    $response['spellcheck']['suggestions'] = [
      'artz',
      [
        'suggestion' =>
          [
            ['word' => "arts", 'freq' => 39026],
            ['word' => "arth", 'freq' => 677],
            ['word' => "arti", 'freq' => 176],
            ['word' => "ariz", 'freq' => 90],
          ],
      ],
    ];

    $this->resultSet->method('getExtraData')
      ->willReturn($response);

    $this->viewRequestStub->query->method('all')->willReturn(['query' => 'artz']);
    $this->viewMock
      ->method('getRequest')
      ->willReturn($this->viewRequestStub);

    $result = $this->fusionSpellcheck->render();

    $this->assertStringContainsString('arts', $result['#link']->getText());
  }

  /**
   * Test render spellcheck returns multiple suggestions.
   *
   * It should return suggestions with highest frequencies.
   */
  public function testRenderSpellcheckMultipleSuggestions(): void {
    $response['spellcheck']['suggestions'] = [
      'artz',
      [
        'suggestion' =>
          [
            ['word' => "arts", 'freq' => 39026],
            ['word' => "arth", 'freq' => 677],
            ['word' => "arti", 'freq' => 176],
            ['word' => "ariz", 'freq' => 90],
          ],
      ],
      'departmant',
      [
        'suggestion' =>
          [
            ['word' => "department", 'freq' => 81730],
            ['word' => "departmrnt", 'freq' => 2],
            ['word' => "departments", 'freq' => 18156],
            ['word' => "departement", 'freq' => 333],
          ],
      ],
    ];

    $this->resultSet->method('getExtraData')
      ->willReturn($response);

    $this->viewRequestStub->query->method('all')->willReturn(['query' => 'artz departmant']);
    $this->viewMock
      ->method('getRequest')
      ->willReturn($this->viewRequestStub);

    $result = $this->fusionSpellcheck->render();

    $this->assertStringContainsString('arts department', $result['#link']->getText());
  }

  /**
   * Test render spellcheck returns the suggestion with the highest frequency.
   *
   * It should also retain the original term without a spelling mistake.
   */
  public function testRenderSpellcheckRetainOriginalSearchTerm(): void {
    $response['spellcheck']['suggestions'] = [
      'artz',
      [
        'suggestion' =>
          [
            ['word' => "arts", 'freq' => 39026],
            ['word' => "arth", 'freq' => 677],
            ['word' => "arti", 'freq' => 176],
            ['word' => "ariz", 'freq' => 90],
          ],
      ],
    ];

    $this->resultSet->method('getExtraData')
      ->willReturn($response);

    $this->viewRequestStub->query->method('all')->willReturn(['query' => 'artz department']);
    $this->viewMock
      ->method('getRequest')
      ->willReturn($this->viewRequestStub);

    $result = $this->fusionSpellcheck->render();

    $this->assertStringContainsString('arts department', $result['#link']->getText());
  }

  /**
   * Test render spellcheck does not return a link.
   *
   * This is in the rare chance that the original search query matches the
   * suggested text.
   */
  public function testRenderSpellcheckOriginalMatchesSuggestion(): void {
    // Since it's hard to come up with a real example, let's assume searching
    // for "artz galleryz" returns "artz" and "galleryz" as the top suggestions.
    $response['spellcheck']['suggestions'] = [
      'artz',
      [
        'suggestion' =>
          [
            ['word' => "artz", 'freq' => 39026],
            ['word' => "arth", 'freq' => 677],
            ['word' => "arti", 'freq' => 176],
            ['word' => "ariz", 'freq' => 90],
          ],
      ],
      'galleryz',
      [
        'suggestion' =>
          [
            ['word' => "galleryz", 'freq' => 2106],
            ['word' => "galleria", 'freq' => 38],
            ['word' => "gallerie", 'freq' => 8],
          ],
      ],
    ];

    $this->resultSet->method('getExtraData')
      ->willReturn($response);

    $this->viewRequestStub->query->method('all')->willReturn(['query' => 'artz galleryz']);
    $this->viewMock
      ->method('getRequest')
      ->willReturn($this->viewRequestStub);

    $this->fusionSpellcheck->render();

    $this->assertEmpty($this->fusionSpellcheck->render());
  }

  /**
   * Test render spellcheck when no suggestion was provided.
   */
  public function testRenderSpellcheckNoSuggestion() : void {
    $response['spellcheck']['suggestions'] = [];
    $this->resultSet->method('getExtraData')
      ->willReturn($response);

    $this->assertEmpty($this->fusionSpellcheck->render());
  }

  /**
   * Test render spellcheck when invalid suggestions are provided.
   */
  public function testRenderSpellcheckWhenSuggestionIsNotAnArray() : void {
    $response['spellcheck']['suggestions'] = 'arts';
    $this->resultSet->method('getExtraData')
      ->willReturn($response);

    $this->viewRequestStub->query->method('all')->willReturn([]);
    $this->viewMock
      ->method('getRequest')
      ->willReturn($this->viewRequestStub);

    $this->assertEmpty($this->fusionSpellcheck->render());
  }

  /**
   * Test render spellcheck when invalid suggestions are provided.
   */
  public function testRenderSpellcheckWhenSuggestionsDataHasNoSuggestions() : void {
    $response['spellcheck']['suggestions'] = [
      'tom',
      [
        'suggestion' => [],
      ],
    ];
    $this->resultSet->method('getExtraData')
      ->willReturn($response);

    $this->viewRequestStub->query->method('all')->willReturn([]);
    $this->viewMock
      ->method('getRequest')
      ->willReturn($this->viewRequestStub);

    $this->assertEmpty($this->fusionSpellcheck->render());
  }

}
