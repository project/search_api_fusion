<?php

/**
 * @file
 * Views hook implementations for the Search API Fusion module.
 */

/**
 * Implements hook_views_data().
 */
function search_api_fusion_views_data(): array {
  $data['views']['search_api_fusion_spellcheck'] = [
    'title' => t('Search API Fusion Spellcheck'),
    'help' => t('Spellcheck suggestions from Fusion response'),
    'area' => [
      'id' => 'search_api_fusion_spellcheck',
    ],
  ];
  $data['views']['search_api_fusion_landing_pages'] = [
    'title' => t('Search API Fusion Landing Pages'),
    'help' => t('Landing Pages from Fusion response'),
    'area' => [
      'id' => 'search_api_fusion_landing_pages',
    ],
  ];
  return $data;
}
