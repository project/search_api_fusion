# Contents of this file

- Introduction
- Requirements
- Recommended modules
- Installation
- Configuration

# Introduction

The Search API Fusion module provides a [Lucidworks Fusion](https://lucidworks.com/products/fusion/) backend for the 
[Search API](https://www.drupal.org/project/search_api) module.

All requests are routed to the [Solr API](https://doc.lucidworks.com/fusion/5.5/313/solr-api) except:
- Search and autocomplete queries which are routed to the [Query API](https://doc.lucidworks.com/fusion/5.5/361/query-api)
- Signals are routed to the [Signals API](https://doc.lucidworks.com/fusion-server/4.2/314/signals-api)

For more information:
- Visit the project page:https://www.drupal.org/project/search_api_fusion
- To submit bug reports and feature suggestions, or track changes: https://www.drupal.org/project/issues/search_api_fusion

# Features supported

- Autocompletion based on Query Pipelines (requires the module [search_api_autocomplete](https://www.drupal.org/project/search_api_autocomplete))
- Spell checking (requires the module [search_api_spellcheck](https://www.drupal.org/project/search_api_spellcheck)
- Faceting (requires the module [facetapi](http://drupal.org/project/facetapi) in Drupal 7 and [facets](https://www.drupal.org/project/facets) in Drupal 8+)
- [Landing pages](https://doc.lucidworks.com/fusion/5.5/285/landing-pages-stage) (promoted results)
- [Request and Click Signals[(https://doc.lucidworks.com/fusion-ai/4.0/user-guide/signals/signals.html)

# Requirements

This module requires the following modules:
- [Search API](http://drupal.org/project/search_api)
- [Search API Solr](http://drupal.org/project/search_api_solr) since Fusion is based on [Apache Solr](http://lucene.apache.org/solr/)
- [Sarnia](https://www.drupal.org/project/sarnia)

# Recommended modules

- [Search API Autocomplete](https://www.drupal.org/project/search_api_autocomplete): required if you want to enable autocomplete feature
- [Search API Spellcheck](https://www.drupal.org/project/search_api_spellcheck): required if you want to enable the spell checking feature.
- Facet modules are required if you want to enable the filtering feature.
  - [Facet API](https://www.drupal.org/project/facetapi) in Drupal 7
  - [Facets](https://www.drupal.org/project/facets) in Drupal 8+


# Installation

- Install as you would normally install a contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further information.
- Composer:  `composer require drupal/search_api_solr`

# Configuration

## Drupal 7

1. Enable the *Search API Fusion* module, its requirements and any recommended module
2. Go to `admin/config/search/search_api` and add a new server with service class `Sarnia Fusion service` - Sarnia should automatically generate a read-only index under this server
3. You can now create a view or a page based on this index, using either the [Search API View](https://www.drupal.org/project/search_api) or [Search API Page](https://www.drupal.org/project/search_api_page) module
4. To enable sending signals to Fusion go to `admin/config/search/search_api/server/{you_server_name}/edit` and check the *Send signals to Fusion* checkbox

## Drupal 9

1. Enable the *Search API Fusion* module, its requirements and any recommended module
2. Go to `/admin/config/search/search_api` and add a new server with backend `Solr` and Solr connector `Fusion`. Provide a Fusion [app](https://doc.lucidworks.com/how-to/847/create-an-app) and [search query profile](https://doc.lucidworks.com/fusion/5.3/174/query-profiles)
3. Create a search index based on the search server you just created ; if your Fusion index is populated via a Fusion index pipeline or some other process outside of Druapl then you may want to make it read-only
4. You can now create a search view at `/admin/structure/views`
5. Configure the user permissions at Administration » People » Permissions
  - Send signals to any Fusion server
  - Signals will be sent to Fusion only if the user performing a search has this permission
6. For autocompletion: enable [search_api_autocomplete](https://www.drupal.org/project/search_api_autocomplete), open the Search API Index you created and click on the `Autocomplete` tab and add a suggester `Fusion query profile`
7. To enable [Click Signals](https://doc.lucidworks.com/fusion-ai/4.2/466/signals-types-and-structures#click):
  1. Go to your server configuration page: `/admin/config/search/search-api/server/{fusion_server_name}/edit`
  2. In the "Configure Fusion Solr Connector" section check "Send click signals to Fusion"
8. To enable [Request Signals](https://doc.lucidworks.com/fusion-ai/4.2/466/signals-types-and-structures#request) enable the Request Signal processor:
  1. Go to your Index processors tab: `/admin/config/search/search-api/index/{search_api_index_name}/processors`
  2. Check "Send request signal to Fusion"
